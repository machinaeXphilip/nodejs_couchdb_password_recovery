# COUCHDB nodejs password recovery helper #

A simple and rough base for a password recovery help for couchDB databases for a nodejs express webserver.

## INSTALL

1. make sure you have couchdb (https://couchdb.apache.org) installed and running. Add an empty database with admin only Read/Write permissions called "recovery".

2. clone or download this repository
3. $ `cd nodejs_couchdb_password_recovery`
4. $ `npm instal`
5. edit server_config.json.example & remove .example from filename
6. $`node index.js`
7. should run now on your machine and can be reached via entrypoint `/recovery` (e.g. http://localhost:8080/recovery)


## CONFIG

ìndex.js will look for a file called `server_config.json`

there is an example file provided `server_config.json.example` that can be used as a template for the server config.

### SSL

by default, SSL is deactivated and your SSL credentials provided in `server_config.json` will be ignored.

to activate SSL, you will need to uncomment the following lines:

line 35+:
```

const options = {
  key: fs.readFileSync(config.SSLkey),
  cert: fs.readFileSync(config.SSLcert)
};

```

line 252+:
```

https.createServer(options, app)
.listen(API_PORT,()=>console.log('DINGSDA recovery API listening on port '+ API_PORT));

```

depending on your server setup you might also have to comment out 

line 249+:
```
http.createServer(app)
.listen(API_PORT,()=>console.log('DINGSDA recovery API listening on port '+ API_PORT));

```

to deactivate all no https requests.


## TODO

- make SSL activate via config or command line parameter
- validate email address
- document data flow and endpoints in README
- implement interval for tidying up recovery database from expired entries