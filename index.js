/**
 * DINGSDA.org Password Recovery Server
 * 
 * this is the first proof of concept for the dingsda thing sharing platforms
 * password recovery tool
 * it is supposed to run alongside the actual API and only manage the data
*/

const config = require("./server_config.json"); // location of config.json

const DBadminPW = config.DBadminPW; // couchDB password for the admin entry

const https = require("https");
const http = require("http");
const fs = require("fs");
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const randomstring = require("randomstring");
const nodemailer = require('nodemailer');

/*
whitelist of domains for Cross-Origin-Requests (CORS)
*/
const whitelist = config.CORS_whitelist.concat(undefined);

const INSTANCEURL = config.instanceURL; // external url leading to this server lowest level (no ports)
const DATABASEURL = config.databaseURL; // url leading to couchDB including port! (probably localhost)
const API_PORT = config.API_PORT; // PORT for API

/*
SSL Stuff
*/
/*
const options = {
  key: fs.readFileSync(config.SSLkey),
  cert: fs.readFileSync(config.SSLcert)
};
*/

/*
email stuff
*/
const email = config.email;


/*
all nano operations are done by either NANO_ADMIN if requestLevel is instance (see README)
or several DBs or by a short term conncetion nanoUser if requestLevel is thing.
we will use another nano Object. This is not needed but human error, ya know...
*/
const NANO_ADMIN = require('nano')('http://admin:'+DBadminPW+'@'+DATABASEURL.split("://")[1]);

const MAXAGE_RECOVERY = 3600000; // 1h

/*///////////////////////////////////////
vvvvvvvvvvvvvvvvvvvvvvvvvv
EXPRESS SERVER:
vvvvvvvvvvvvvvvvvvvvvvvvvv
///////////////////////////////////////*/

const app = express()

app.use(cors({origin: function (origin, callback) {
    console.log("EXPRESS CORS check","origin:",origin); 
    
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Origin '+origin+' not allowed by CORS'))
    }
  },credentials: true}))

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({limit: '1mb',extended: true}));

app.use(express.json()); // after this req.body should contain json

app.use(cookieParser());

app.get("/recovery",async (req, res) => {
    
    console.log("RECOVERY MAIN SITE");

    let website = makeWebsiteMain({url:INSTANCEURL+"recoverycreate"})
    res.send(website);

})

app.post("/recoverycreate/:username/:email",async (req, res) => {
    
    console.log("RECOVERY CREATE LINK REQUEST");
    console.log(req.params);  
    
    let username = req.params["username"];
    console.log(username);
    
    // check if user exists
    let userdata = await NANO_ADMIN.use('_users').get("org.couchdb.user:"+username)
        .catch((err)=>{`could not get userdata from Db: ${err}`})
    if (!userdata){ return res.send("nope. user does not exist")}
    //console.log(userdata);
    // check if user email correct and known
    if(!userdata.email){ return res.send("this user has no email in our database. please contact our admin")}
    console.log(userdata.email);
    
    // create DB entry with random token
    let token = randomstring.generate({length:96});
    console.log(token);
    await NANO_ADMIN.use('recovery').insert(
        {
            "_id": token,
            "username": username,
            "timestamp": Date.now()
        }    
    )
    .catch((err)=>{`could not insert new recoverypage: ${err}`})
    
    // send email with token link to user
    // UNCOMMENT TO ACTIVATE!
    ///*
    sendemail(
        userdata.email,
        "dingsda password recovery",
        `Hi, we received a request to reset your password on dingsda.org. 
        If this was you, please follow this link to reset your password. 
        If not, ignore this email. 
        ${config.instanceURL}recoverylinks/${token}
        `,
        `Hi, we received a request to reset your password on dingsda.org. 
        If this was you, please follow this link to reset your password. 
        If not, ignore this email.<br><br>
        <a href="${config.instanceURL}recoverylinks/${token}">${config.instanceURL}recoverylinks/${token}</a>
        `
    ).catch(console.error);
    //*/

    res.send("you will receive a link to the email provided (if you exist)");

})

/**
 * ENDPOINT /recoverylinks/{{singleUseToken}}
 * 
 * here the user can change their password ONCE by providing username and new pw
 * the UI displayed here only consists of the return value of makeWebsite()
 * this should send the user and pw to ENDPOINT /recoveryhelp
 * 
 * link to this endpoint (aka the singleUseToken) should have been created by ENDPOINT /recoveryCreate
 */
app.get("/recoverylinks/*",async (req, res) => {
    
    console.log("RECOVERY LINK clicked!");
    console.log(req.params);  
    
    let recoveryToken = req.params["0"];
    console.log(recoveryToken);
    
    // check if recoveryToken can be found within DB "recovery"
    let recoveryData = await NANO_ADMIN.use('recovery').get(recoveryToken)
        .catch((err)=>{`could not get recoverydata: ${err}`})
    
    if (
        !recoveryData ||
        !recoveryData.username
    ){
        return res.send("this link has no power here")
    }
    if (
        !recoveryData.timestamp ||
        recoveryData.timestamp + MAXAGE_RECOVERY < Date.now()
    ){
        console.log("expired!!!");        
        // delete DB entry 
        await NANO_ADMIN.use('recovery').destroy(recoveryToken,recoveryData._rev)
        .catch((err)=>{return res.send("error while deleting old one")})
        return res.send("this link expired")
    }
    
    console.log(recoveryData.timestamp + MAXAGE_RECOVERY,Date.now());
    
    console.log(recoveryData);
    
    let website = makeWebsite({url:INSTANCEURL+"recoverylinks",recoveryToken:recoveryToken})
    res.send(website);

})


app.post("/recoverylinks/:recoveryToken/:username/:password",async (req, res) => {
    
    console.log("RECOVERY ATTEMPT");
    
    let recoveryToken = req.params["recoveryToken"];
    let username = req.params["username"];
    let password = req.params["password"];
    console.log("/recoverhelp:",recoveryToken,username,password);
    
    // check if recoveryToken can be found within DB "recovery"
    let recoveryData = await NANO_ADMIN.use('recovery').get(recoveryToken)
        .catch((err)=>{`could not get recoverydata: ${err}`})
    
    if (
        !recoveryData ||
        !recoveryData.username
    ){
        return res.send("this link has no power here")
    }
    if ( recoveryData.username !== username){
        return res.send("its not you! impostor!")
    }
    if (
        !recoveryData.timestamp || recoveryData.timestamp + MAXAGE_RECOVERY < Date.now()
    ){
        console.log("expired!!!");
        await NANO_ADMIN.use('recovery').destroy(recoveryToken,recoveryData._rev)
        .catch((err)=>{return res.send("error while deleting old one")})        
        return res.send("this link expired")
    }
    
    // try to update pw as ADMIN
    let userdata = await NANO_ADMIN.use('_users').get("org.couchdb.user:"+username)
        .catch((err)=>{return res.send("error while fetching userdata")})
    
    if (userdata)
    {
        userdata.password = password;
        await NANO_ADMIN.use('_users').insert(userdata)
        .catch((err)=>{return res.send("error while updating userdata")})
    }

    // delete DB entry
    await NANO_ADMIN.use('recovery').destroy(recoveryToken,recoveryData._rev)
    .catch((err)=>{return res.send("error while deleting old one")})
    res.send("successful. we will try to update your password")
})


app.all("*", (req,  res) => {

    //console.log(req.body);    
    res.send("this link has no power here")


})


http.createServer(app)
.listen(API_PORT,()=>console.log('DINGSDA recovery API listening on port '+ API_PORT));

/*
https.createServer(options, app)
.listen(API_PORT,()=>console.log('DINGSDA recovery API listening on SSL port '+ API_PORT));
*/

/*///////////////////////////////////////
^^^^^^^^^^^^^^^^^^^^^^^^^^
EXPRESS SERVER END
^^^^^^^^^^^^^^^^^^^^^^^^^^
///////////////////////////////////////*/


/*/////////////////////////
vvvvvvvvvvvvvvvvvvvvvvvvvv
GENERAL HELPER FUNCTIONS
vvvvvvvvvvvvvvvvvvvvvvvvvv
//////////////////////////*/

/**
helper function<br>
<br>
turns string to Hex value

@param {string} s - string to be transformed
*/
function toHex(s) {
    // utf8 to latin1
    var s = unescape(encodeURIComponent(s))
    var h = ''
    for (let i = 0; i < s.length; i++) {
        h += s.charCodeAt(i).toString(16)
    }
    return h
}



function makeWebsite(options){
    if (!options.passwordMinimalLength){options.passwordMinimalLength = 8}
    return `
    <html>
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <style>

    body {display:flex;flex-direction:column; font-family: Arial;justify-content: center;}
    h2 {font-size:2em; color: lightblue}
    input{font-size:2em;}
    button{font-size:2em; background-color:lightblue}

    @media only screen and (max-width: 767px) {
        body {display:flex;flex-direction:column; font-family: Arial;justify-content: center;}
        h2 {font-size:1em; color: lightblue}
        input{font-size:2em;}
        button{font-size:2em; background-color:lightblue}
    }
    </style>
    
    <h2>DINGSDA PASSWORD RESET</h2><hr>
    <input type="text" id="username" placeholder="username">
    <br>
    <input type="password" id="password" placeholder="new password">
    <br>
    <button id="send">SEND</button>

    <br>
    <br>
    <span id="feedback"></span>
    

    <script>

    document.getElementById("send").addEventListener("click",()=>{

        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;

        if (!username || !password){
            document.getElementById("feedback").innerHTML = "Please provide your username and a new password!";
            return 
        }
        if (password.length < ${options.passwordMinimalLength}){
            document.getElementById("feedback").innerHTML = "Password has to have at least ${options.passwordMinimalLength} characters";
            return 
        }

        document.getElementById("send").disabled = true;
        
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
            document.getElementById("feedback").innerHTML = this.responseText;
            }
        };
        xhttp.onerror = function () {
            document.getElementById("feedback").innerHTML = "something went wrong. please try again";
        }
        xhttp.open("POST", "${options.url}/${options.recoveryToken}/"+username+"/"+password, true);
        xhttp.send();
    })

    </script>

    </html>
    `
}

function makeWebsiteMain(options){
    return `
    <html>
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <style>

    body {display:flex;flex-direction:column; font-family: Arial;justify-content: center;}
    h2 {font-size:2em; color: lightblue}
    input{font-size:2em;}
    button{font-size:2em; background-color:lightblue}

    @media only screen and (max-width: 767px) {
        body {display:flex;flex-direction:column; font-family: Arial;justify-content: center;}
        h2 {font-size:1em; color: lightblue}
        input{font-size:2em;}
        button{font-size:2em; background-color:lightblue}
    }
    </style>
    
    <h2>YOU FORGOT YOUR PASSWORD?</h2><hr>
    <input type="text" id="username" placeholder="username">
    <br>
    <input type="text" id="email" placeholder="email">
    <br>
    <button id="send">SEND</button>

    <br>
    <br>
    <span id="feedback"></span>
    

    <script>

    document.getElementById("send").addEventListener("click",()=>{

        let username = document.getElementById("username").value;
        let email = document.getElementById("email").value;

        if (!username || !email){
            document.getElementById("feedback").innerHTML = "Please provide your username and a new email!";
            return 
        }

        document.getElementById("send").disabled = true;

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
            document.getElementById("feedback").innerHTML = this.responseText;
                }
        };
        xhttp.open("POST", "${options.url}/"+username+"/"+email, true);
        xhttp.send();
    })

    </script>

    </html>
    `
}


async function sendemail(to,subject,text,html) {

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: email.host,
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: email.auth.user, // generated ethereal user
            pass: email.auth.pass // generated ethereal password
        }
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: `"dingsda password recovery" <${email.address}>`, // sender address
        to: to,//'info@philipsteimel.de', // list of receivers
        subject: subject,//'Hello ✔', // Subject line
        text: text,//'Hello world?', // plain text body
        html: html,//'<b>Hello world?</b>' // html body
    });

    console.log('Message sent: %s', info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

}